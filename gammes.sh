#!/bin/bash
# utilise le programme play du paquet sox

# - musique - #
DO[0]=32.70;   DOd[0]=34.65;   RE[0]=36.71;   REd[0]=38.89;   MI[0]=41.20;   FA[0]=43.65;   FAd[0]=46.25;   SOL[0]=49.00;   SOLd[0]=51.91;   LA[0]=55.00;   LAd[0]=58.27;   SI[0]=61.74;
DO[1]=65.41;   DOd[1]=69.30;   RE[1]=73.42;   REd[1]=77.78;   MI[1]=82.41;   FA[1]=87.31;   FAd[1]=92.50;   SOL[1]=98.00;   SOLd[1]=103.83;  LA[1]=110.00;  LAd[1]=116.54;  SI[1]=123.47;
DO[2]=130.81;  DOd[2]=138.59;  RE[2]=146.83;  REd[2]=155.56;  MI[2]=164.81;  FA[2]=174.61;  FAd[2]=185.00;  SOL[2]=196.00;  SOLd[2]=207.65;  LA[2]=220.00;  LAd[2]=233.08;  SI[2]=246.94;
DO[3]=261.63;  DOd[3]=277.18;  RE[3]=293.66;  REd[3]=311.13;  MI[3]=329.63;  FA[3]=349.23;  FAd[3]=369.99;  SOL[3]=392.00;  SOLd[3]=415.30;  LA[3]=440.00;  LAd[3]=466.16;  SI[3]=493.88;
DO[4]=523.25;  DOd[4]=554.37;  RE[4]=587.33;  REd[4]=622.25;  MI[4]=659.26;  FA[4]=698.46;  FAd[4]=739.99;  SOL[4]=783.99;  SOLd[4]=830.61;  LA[4]=880.00;  LAd[4]=932.33;  SI[4]=987.77;
DO[5]=1046.50; DOd[5]=1108.73; RE[5]=1174.66; REd[5]=1244.51; MI[5]=1318.51; FA[5]=1396.91; FAd[5]=1479.98; SOL[5]=1567.98; SOLd[5]=1661.22; LA[5]=1760.00; LAd[5]=1864.66; SI[5]=1975.53;
DO[6]=2093.00; DOd[6]=2217.46; RE[6]=2349.32; REd[6]=2489.02; MI[6]=2637.02; FA[6]=2793.83; FAd[6]=2959.96; SOL[6]=3135.96; SOLd[6]=3322.44; LA[6]=3520.00; LAd[6]=3729.31; SI[6]=3951.07;
DO[7]=4186.01; DOd[7]=4434.92; RE[7]=4698.64; REd[7]=4978.03; MI[7]=5274.04; FA[7]=5587.65; FAd[7]=5919.91; SOL[7]=6271.93; SOLd[7]=6644.88; LA[7]=7040.00; LAd[7]=7458.62; SI[7]=7902.13;

function gamme () {
    fctIn "$BASH_SOURCE:$*"
    local -i delai=0.15
    #for gamme in 0 1 2 3 4 5 6 7
    for gamme in 7
    do
        echo "gamme: $gamme; note  ${DO[gamme]} ${DOd[gamme]} ${RE[gamme]} ${REd[gamme]} ${MI[gamme]} ${FA[gamme]} ${FAd[gamme]} ${SOL[gamme]} ${SOLd[gamme]} ${LA[gamme]} ${LAd[gamme]} ${SI[gamme]}"
        echo " note  DO   ${DO[gamme]}"; play --no-show-progress  --null synth $delai pluck ${DO[gamme]}
        echo " note  DO#  ${DOd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${DOd[gamme]}
        echo " note  RE   ${RE[gamme]}"; play --no-show-progress  --null synth $delai pluck ${RE[gamme]}
        echo " note  RE#  ${REd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${REd[gamme]}
        echo " note  MI   ${MI[gamme]}"; play --no-show-progress  --null synth $delai pluck ${MI[gamme]}
        echo " note  FA   ${FA[gamme]}"; play --no-show-progress  --null synth $delai pluck ${FA[gamme]}
        echo " note  FA#  ${FAd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${FAd[gamme]}
        echo " note  SOL  ${SOL[gamme]}"; play --no-show-progress  --null synth $delai pluck ${SOL[gamme]}
        echo " note  SOL# ${SOLd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${SOLd[gamme]}
        echo " note  LA   ${LA[gamme]}"; play --no-show-progress  --null synth $delai pluck ${LA[gamme]}
        echo " note  LA#  ${LAd[gamme]}"; play --no-show-progress  --null synth $delai pluck ${LAd[gamme]}
        echo " note  SI   ${SI[gamme]}"; play --no-show-progress  --null synth $delai pluck ${SI[gamme]}
    done
    fctOut "$FUNCNAME";return 0;
}

function carillon1(){
    fctIn "$BASH_SOURCE:$*"
    local -i gamme=5;
    local -i delai=1;
    play --no-show-progress  --null synth $delai pluck ${MI[gamme]}
    play --no-show-progress  --null synth $delai pluck ${MI[gamme-1]}
    play --no-show-progress  --null synth $delai pluck ${MI[gamme]}
    fctOut "$FUNCNAME";return 0;
}

